import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store =  new Vuex.Store({
  state: {
    selectedClient: 'null',
    clientBuilding: [],
    bName: '',
    location: ''

  },
  mutations: {
    setBuildingName(state, name){
      state.bName = name;
    },
    setBLocation(state, location){
      state.location = location;
    },
    setClient(state, client){
      state.selectedClient = client; 
    },
    setClientBuilding(state, clientBuildings){
      state.clientBuilding = clientBuildings
    }
  },
  getters: {
    getBuildingName(state){
      return state.bName;
    },
    getBLocation(state){
      return state.location;
    },
    getSelectedClient(state){
      return state.selectedClient;
    },
    getClientsBuilding(state){
      return state.clientBuilding;
    }
  },
  actions: {
  },
  modules: {
  }
})

export default store;