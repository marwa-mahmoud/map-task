import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
// import AddBuildingForm from '../components/AddBuildingForm.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/:cityId',
    name: 'Home',
    component: Home
  },
  {
    path: '/add-building-form',
    name: 'AddBuildingForm',
    component: () => import(/* webpackChunkName: "about" */ '../components/AddBuildingForm.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
